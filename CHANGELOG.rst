This document contains the fsleyes-plugin-mrs release history in reverse chronological order.

v0.1.0 (Monday 10th July 2023)
------------------------------
- Better handling and viewing of xnuclear spectroscopy, specifically 31P, 13C, and 2H.
- Added ability to view mean spectra over each higher NIfTI-MRS dimension.
- Added ability to view difference spectra over higher dimensions with two elements.
- Updated documentation

v0.0.8 (2nd March 2023)
-----------------------
- Fixed layout bug for newer versions of fsleyes
- Enabled loading of plugin manually.

v0.0.7 (6th July 2021)
----------------------
- Include LICENSE file in source release.

v0.0.6 (6th July 2021)
----------------------
- Updated documentation and descriptions for PyPI and Conda Forge release.

v0.0.5 (6th July 2021)
----------------------
- Automatic scaling of chemical shift axis now linked to autoscalex button.
- fft of complex FID now incorporates appropriate scaling of first FID point.

v0.0.4 (3rd June 2021)
----------------------
- Interactive phasing added to MRS view panel.

v0.0.3 (3rd June 2021)
----------------------
- Colour bar automatically added to orthographic view when results are loaded.
- Annotations are now added to the orthographic view to indicate where held spectra are located.
- Requires v1.0.10 of FSLeyes

v0.0.2 (1st June 2021)
----------------------
- Added fsl_mrsi results interface
- Added documentation and documentation page.

v0.0.1 (11th May 2021)
----------------------
- Initial release
- Automatic scaling, orientation and referencing of spectra. 
- Viewing for higher NIfTI-MRS dimensions.
