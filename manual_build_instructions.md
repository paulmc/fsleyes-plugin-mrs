# Manual build process
1. Update version in setup.py
2. Update changelog.rst
3. Commit, push and run tests
4. git tag -m VX.X.X X.X.X
5. git push github master --tags
6. git clean -fdxn -e .vscode -e test_data
7. git clean -fdx -e .vscode -e test_data
8. python setup.py sdist
9. python setup.py bdist_wheel
10. python -m twine upload dist/*