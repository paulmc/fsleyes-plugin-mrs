.. fsleyes-plugin-mrs documentation master file, created by
   sphinx-quickstart on Fri May 28 16:19:23 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

FSLeyes MRS Plugin Documentation
================================

.. image:: img/avatar.png
   :scale: 25%
   :align: center

This is the user documentation for the FSLeyes Magnetic
Resonance Spectroscopy (MRS) Plugin (fsleyes-plugin-mrs).
*FSLeyes* is the the `FSL <http://fsl.fmrib.ox.ac.uk/fsl/fslwiki/>`_ image viewer. This documentation pertains to fsleyes-plugin-mrs |version|.

.. toctree::
   :name: mastertoc
   :maxdepth: 1
   :caption: Contents:

   install
   viewing
   manipulating
   mrsi_results
   changelog
