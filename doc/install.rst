Installing fsleyes-plugin-mrs
=============================

Requirements
~~~~~~~~~~~~
This package requires a minimum of FSLeyes 1.0.5. 
Please see the `instructions for installing FSLeyes <https://open.win.ox.ac.uk/pages/fsl/fsleyes/fsleyes/userdoc/install.html>`_

Installation
~~~~~~~~~~~~
You can install *fsleyes-plugin-mrs* using one of three methods: GitLab, Conda, or Pypi.

In both cases the *fsleyes-plugin-mrs*  package must be installed into the same environment as FSLeyes is being run from. We recommend using the `conda <https://docs.conda.io/en/latest/miniconda.html>`_ package manager to achieve this.

For more information on loading fsleyes plugins please see the `FSLeyes documentation <https://open.win.ox.ac.uk/pages/fsl/fsleyes/fsleyes/userdoc/plugins.html#loading-installing-fsleyes-plugins>`_

GitLab
------
To install this plugin from GitLab, after activating the target environment (``conda activate my_environment``) run::

    git clone https://git.fmrib.ox.ac.uk/wclarke/fsleyes-plugin-mrs
    cd fsleyes-plugin-mrs
    pip install .

Conda
-----
To install this plugin from conda, after activating the target environment (``conda activate my_environment``) run::

    conda install -c conda-forge fsleyes-plugin-mrs

PyPI
----
To install this plugin from PyPI run::
    
    pip install fsleyes-plugin-mrs