Manipulating Spectra
====================

Interactive Phasing of Spectra
------------------------------
Phasing of spectra can be carried out manually using the *MRS View* **control panel** (open with |mrs_ctrl_icon|, or by the `Settings -> MRS N -> MRS control` menu option). Zero and first-order phase can be applied using the relevant fields in the menu. This must be done on each spectrum individually.

.. |mrs_ctrl_icon| image:: img/mrs_ctrl_panel_icon.png
    :width: 24

.. image:: img/mrs_ctrl_panel.png
    :width: 300
    :align: center

Phasing can also be carried out interactively in the *MRS View*. Left and right click without any modifier keys will pan and zoom the spectrum view respectively. If left click and pan is combined with the **control**, **shift** (or both) keys then the selected overlay spectrum will be phased. Zero-order and first-order phasing can be applied separately or together. Adding the **Alt** key phases all visible data series (which aren't pinned by the *Plot list* panel).

.. list-table:: Interactive Phasing Key Combinations & Actions
   :widths: 30 30
   :header-rows: 1
   :align: center

   * - Key Combination
     - Action
   * - Left click (l/r/u/d)
     - Pan view
   * - Right click (l/r/u/d)
     - Zoom view
   * - Left click (l/r) + ctrl
     - Zero-order phase 
   * - Left click (u/d) + shift
     - First-order phase
   * - Left click (l/r/u/d) + ctrl + shift
     - Both
   * - Left click (l/r) + ctrl + Alt
     - Zero-order phase (all spectra)
   * - Left click (u/d) + shift + Alt
     - First-order phase (all spectra)
   * - Left click (l/r/u/d) + ctrl + shift + Alt
     - Both (all spectra)

Pinning Multiple Spectra
------------------------
Multiple spectra from the same or different datasets can be compared directly in the MRS view by pinning the currently displayed spectrum using the **Plot list** panel.

Clicking the '**+**' button on either the *Plot list* panel or the main *MRS view toolbar* will hold a spectrum (and add it to the list of displayed spectra in the *Plot list* panel). Pinned spectra can be removed using the '**-**' buttons.

.. image:: img/pin_spectra.png

Whenever a spectrum is pinned, an annotation mark will be placed on each view in the orthographic panel marking the spatial origin of the spectrum. The colour of the mark will match (and dynamically update with) that of the spectrum in the *MRS view*.

The style of mark can be modified using the Annotations panel (`Settings -> Ortho view N -> Annotations`).
